/**
 * @file 
 * @brief Implementation of the V0 of the system project.
 * @warning You should not modifie the current file.
 */

#include "project.h"
#include <signal.h>
#include <time.h>
#include <pthread.h>
#include <assert.h>

/**
 * @brief Maximum length (in character) for a file name.
 **/
#define PROJECT_FILENAME_MAX_SIZE 1024

/**
 * @brief Type of the sort algorithm used in this version.
 **/
//#define SORTALGO(nb_elem, values) SU_ISort(nb_elem, values)
//#define SORTALGO(nb_elem, values) SU_HSort(nb_elem, values)
#define SORTALGO(nb_elem, values) SU_QSort(nb_elem, values)

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER; /* Création du mutex */

T_maillon * filename_sort_Struct;
/**********************************/

void projectV4(const char *i_file, const char *o_file, unsigned long nb_split)
{
    filename_sort_Struct= (T_maillon *)malloc(sizeof(T_maillon));

    /* Get number of line to sort */
    int nb_print = 0;
    unsigned long nb_lines = SU_getFileNbLine(i_file);
    unsigned long nb_lines_per_files = nb_lines / (unsigned long)nb_split;
    fprintf(stderr,
            "Projet4 version with %lu split of %lu lines\n",
            nb_split,
            nb_lines_per_files);

    /* 0 - Deal with case nb_split = 1 */
    if (nb_split < 2)
    {
        int *values = NULL;
        unsigned long nb_elem = SU_loadFile(i_file, &values);

        SORTALGO(nb_elem, values);

        SU_saveFile(o_file, nb_elem, values);

        free(values);
        return;
    }
    /* 1 - Split the source file */

    /* 1.1 - Create a vector of target filenames for the split */
    char **filenames = (char **)malloc(sizeof(char *) * (size_t)nb_split);
    char **filenames_sort = (char **)malloc(sizeof(char *) * (size_t)nb_split);
    unsigned long cpt = 0;
    for (cpt = 0; cpt < nb_split; ++cpt)
    {
        filenames[cpt] = (char *)malloc(sizeof(char) * PROJECT_FILENAME_MAX_SIZE);
        nb_print = snprintf(filenames[cpt],
                            PROJECT_FILENAME_MAX_SIZE,
                            "/tmp/tmp_split_%d_%lu.txt", getpid(), cpt);
        if (nb_print >= PROJECT_FILENAME_MAX_SIZE)
        {
            err(1, "Out of buffer (%s:%d)", __FILE__, __LINE__);
        }

        filenames_sort[cpt] = (char *)malloc(sizeof(char) * PROJECT_FILENAME_MAX_SIZE);
        nb_print = snprintf(filenames_sort[cpt],
                            PROJECT_FILENAME_MAX_SIZE,
                            "/tmp/tmp_split_%d_%lu.sort.txt", getpid(), cpt);
        if (nb_print >= PROJECT_FILENAME_MAX_SIZE)
        {
            err(1, "Out of buffer (%s:%d)", __FILE__, __LINE__);
        }
    }

    /* 1.2 - Split the source file */
    SU_splitFile2(i_file,
                  nb_lines_per_files,
                  nb_split,
                  (const char **)filenames);

    /* 2 - Sort each file */
    projectV4_sortFiles(nb_split, (const char **)filenames, (const char **)filenames_sort,(const char *)o_file);
    printf("sortie du sort\n");

    /* 3 - Merge (two by two) */
    projectV4_combMerge(nb_split, (const char **)filenames_sort, (const char *)o_file);

    /* 4 - Clear */
    for (cpt = 0; cpt < nb_split; ++cpt)
    {
        free(filenames[cpt]); // not needed :  clear in sort
        free(filenames_sort[cpt]);
    }

    free(filenames);
    free(filenames_sort);
}

typedef struct thread_paramv4{
    const char **filenames;
    const char **filenames_sort;
    unsigned long cpt;
    char* previous_name;
    char* previous_name2;
    char* current_name;
    int nb_print;
    size_t midsub;
} thread_paramv4_t;


void affiche(T_maillon *liste){ 
    if(liste==NULL){printf("null\n");}else{
        printf("element liste : %s\n",liste->info);
    while (liste->suiv!=NULL){
        printf("element liste : %s\n",liste->info);
        liste=liste->suiv;
        
    }
    printf("fin ?\n");
    }
    
}

void ajoutQueue(T_maillon **ptl, char *  i){
    T_maillon *q, *p;
    q = (T_maillon *)malloc(sizeof(T_maillon));
    //assert(q != null);
    q->info =(char*)malloc(50*sizeof(char));
    strcpy(q->info, i);
    printf("q->info : %s\n",q->info);
    q->suiv = NULL;
    if((*ptl)->suiv==NULL){
		*ptl = q;
    } else {
		printf("(*ptl)->suiv!=NULL)\n");
        p=*ptl;
        while(p->suiv != NULL){
			p = p->suiv;
		}
		p->suiv = q;
	}
	printf("test ajout fini\n");
}

char * suppressionTete(T_maillon *liste){
    char *filename;
    filename=(char *)malloc(sizeof(char));
    if(liste==NULL){printf("vide\n"); return NULL;}
    T_maillon *supp;
    supp=liste;
    filename=liste->info;
    liste=liste->suiv;
    free(supp->info);
    free(supp);
    return filename;
}



void *projectV4_thread_sort(void* param){
	thread_paramv4_t* args = (thread_paramv4_t*)param;
	// Processus fils
	int *values = NULL;
	unsigned long nb_elem = SU_loadFile(args->filenames[args->cpt], &values);
	
	SU_removeFile(args->filenames[args->cpt]);
	printf("pere :%d\t fils : %d\n", getppid(), getpid());
	fprintf(stderr, "Inner sort %lu: Array of %lu elem by %d\n", args->cpt, nb_elem, getpid());

	SORTALGO(nb_elem, values);

	SU_saveFile(args->filenames_sort[args->cpt], nb_elem, values);

	pthread_mutex_lock(&mutex); /* On verrouille le mutex */
	printf("test de args :%s\n",( char *)args->filenames_sort[args->cpt]);
	ajoutQueue(&filename_sort_Struct,( char *)args->filenames_sort[args->cpt]);
	printf("test affiche\n");
	affiche(filename_sort_Struct);
	pthread_mutex_unlock(&mutex);
	free(values);
    return NULL;
}

void projectV4_sortFiles(unsigned long nb_split, const char **filenames, const char **filenames_sort,const char *o_file)
{
    unsigned long cpt = 0;
    pthread_t  threadArray[nb_split];    
    for (cpt = 0; cpt < nb_split; ++cpt)
    {
        thread_paramv4_t* thread_paramv4 = (thread_paramv4_t*)malloc(sizeof(thread_paramv4_t));

        thread_paramv4->filenames= filenames;
        thread_paramv4->filenames_sort=filenames_sort;
        thread_paramv4->cpt=cpt;
        if (pthread_create((&threadArray[cpt]),NULL,projectV4_thread_sort,(void *)thread_paramv4)==-1)//(processus = fork()) == 0)
        { 
            perror("Erreur dans pthread_create\n");
            exit(EXIT_FAILURE);
        }
    }
    for(size_t i = 0; i < nb_split; i++)
    {
        if (pthread_join(threadArray[i], NULL)) {
            perror("pthread_join\n");
        }
        if (i%2==0){
            pthread_mutex_lock(&mutex);
            char ** filenames_sort_file =(char **)malloc(sizeof(char *) *30);
            
            filenames_sort_file[0]=suppressionTete(filename_sort_Struct);

            filenames_sort_file[1]=suppressionTete(filename_sort_Struct);

            printf("%s\n",filenames_sort_file[0]);
            if (filenames_sort_file[0] != NULL && filenames_sort_file[1] != NULL){
                projectV0_combMerge(nb_split, (const char **)filenames_sort_file, (const char *)o_file);
                free(filenames_sort_file[0]);
                free(filenames_sort_file[1]);
            }
            pthread_mutex_unlock(&mutex);
        }
        pthread_detach(threadArray[i]);
    }
}

void *projectV4_thread_mergev4_1(void* param){
    thread_paramv4_t* args = (thread_paramv4_t*)param;
    // Processus fils
    printf("if 1\t cp %ld\n", args->cpt);
    printf("cpt : %ld\t previous name  : %s \t current name %s\t filename %s \n",args->cpt,args->previous_name,args->current_name,args->filenames_sort[args->cpt]);

    fprintf(stderr, "Merge sort %lu : %s + %s -> %s \n",
            args->cpt,
            args->previous_name,
            args->filenames_sort[args->cpt],
            args->current_name);
    SU_mergeSortedFiles(args->previous_name,
                        args->filenames_sort[args->cpt],
                        args->current_name);

    SU_removeFile(args->previous_name);
    SU_removeFile(args->filenames_sort[args->cpt]);

    args->nb_print = snprintf(args->previous_name,
                        PROJECT_FILENAME_MAX_SIZE,
                        "%s", args->current_name);
    if (args->nb_print >= PROJECT_FILENAME_MAX_SIZE)
    {
        err(1, "Out of buffer (%s:%d)", __FILE__, __LINE__);
    }
    args->nb_print = snprintf(args->current_name,
                        PROJECT_FILENAME_MAX_SIZE,
                        "/tmp/tmp_split_%d_merge_%lu.txt", getpid(), args->cpt);//(args->infMidSup==1)?args->cpt+1:args->cpt);
    if (args->nb_print >= PROJECT_FILENAME_MAX_SIZE)
    {
        err(1, "Out of buffer (%s:%d)", __FILE__, __LINE__);
    }

    return NULL;
}

void *projectV4_thread_mergev4_2(void* param){
    thread_paramv4_t* args = (thread_paramv4_t*)param;
    // Processus fils
    printf("if 1\t cp %ld\n", args->cpt);
    printf("cpt : %ld\t previous name  : %s \t current name %s\t filename %s \n",args->cpt,args->previous_name2,args->current_name,args->filenames_sort[args->cpt]);

    fprintf(stderr, "Merge sort %lu : %s + %s -> %s \n",
            args->cpt,
            args->previous_name2,
            args->filenames_sort[args->cpt+args->midsub],
            args->current_name);
    SU_mergeSortedFiles(args->previous_name2,
                        args->filenames_sort[args->cpt+args->midsub],
                        args->current_name);

    SU_removeFile(args->previous_name2);
    SU_removeFile(args->filenames_sort[args->cpt+args->midsub]);

    args->nb_print = snprintf(args->previous_name2,
                        PROJECT_FILENAME_MAX_SIZE,
                        "%s", args->current_name);
    if (args->nb_print >= PROJECT_FILENAME_MAX_SIZE)
    {
        err(1, "Out of buffer (%s:%d)", __FILE__, __LINE__);
    }
    args->nb_print = snprintf(args->current_name,
                        PROJECT_FILENAME_MAX_SIZE,
                        "/tmp/tmp_split_%d_merge_%lu.txt", getpid(), args->cpt+1);
    if (args->nb_print >= PROJECT_FILENAME_MAX_SIZE)
    {
        err(1, "Out of buffer (%s:%d)", __FILE__, __LINE__);
    }

    return NULL;
}

void projectV4_combMerge(unsigned long nb_split, const char **filenames_sort, const char *o_file)
{
    
    int nb_print = 0;
    unsigned long cpt = 0;
    long unsigned int midSup = nb_split / 2;
    long unsigned int midSub = nb_split - midSup;
    long unsigned int mid = (midSub < midSup) ? midSup : midSub;
    char previous_name[PROJECT_FILENAME_MAX_SIZE];
    nb_print = snprintf(previous_name,
                        PROJECT_FILENAME_MAX_SIZE,
                        "%s", filenames_sort[0]);
    char previous_name2[PROJECT_FILENAME_MAX_SIZE];
    nb_print = snprintf(previous_name2,
                        PROJECT_FILENAME_MAX_SIZE,
                        "%s", filenames_sort[mid]);
    if (nb_print >= PROJECT_FILENAME_MAX_SIZE)
    {
        err(1, "Out of buffer (%s:%d)", __FILE__, __LINE__);
    }

    char current_name[PROJECT_FILENAME_MAX_SIZE];
    nb_print = snprintf(current_name,
                        PROJECT_FILENAME_MAX_SIZE,
                        "/tmp/tmp_split_%d_merge_%d.txt", getpid(), 0);
    if (nb_print >= PROJECT_FILENAME_MAX_SIZE)
    {
        err(1, "Out of buffer (%s:%d)", __FILE__, __LINE__);
    }

    printf("midsub %ld\t midsup %ld\t mid %ld\n", midSub, midSup, mid);
    pthread_t threadArray[nb_split];
    thread_paramv4_t* thread_paramv4 = (thread_paramv4_t*)malloc(sizeof(thread_paramv4_t));
    thread_paramv4->current_name=current_name; 
    thread_paramv4->filenames_sort=filenames_sort;
    for (cpt = 1; cpt < mid; ++cpt)
    {
    
        thread_paramv4->cpt=cpt;
        
        printf("cpt : %ld\n",cpt);  
        if (cpt < midSub)
        {
            printf("cpt < midSub\n");
            thread_paramv4->previous_name=previous_name;
            if (pthread_create(&(threadArray[cpt]),NULL,projectV4_thread_mergev4_1,(void *)thread_paramv4)==-1)
            { 
                perror("Erreur dans pthread_create\n");
                exit(EXIT_FAILURE);
            }
             if (pthread_join(threadArray[cpt], NULL)) {
            perror("pthread_join\n");
        }

        pthread_detach(threadArray[cpt]);
            
        }
        if (cpt < midSup)
        {
            printf("cpt < midSup\n");
            thread_paramv4->previous_name2=previous_name2;
            
            thread_paramv4->midsub=midSub;
            
            if (pthread_create(&(threadArray[cpt]),NULL,projectV4_thread_mergev4_2,(void *)thread_paramv4)==-1)
            { 
                perror("Erreur dans pthread_create pour cpt < midSup\n");
                exit(EXIT_FAILURE);
            }
            if (pthread_join(threadArray[cpt], NULL)) {
            perror("pthread_join\n");
        }

        pthread_detach(threadArray[cpt]);
            
        }
    }

    /* Last merge */
    fprintf(stderr, "Last merge sort : %s + %s -> %s \n",
            previous_name,
            previous_name2,
            o_file);
   
    SU_mergeSortedFiles(previous_name,
                        previous_name2,
                        o_file);
    
    SU_removeFile(previous_name);
    
    SU_removeFile(previous_name2);
}
