/**
 * @file
 * @brief V0, V1, V2, V3 and V4 of the system project.
 */

#ifndef PROJECT_H
#define PROJECT_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>    /* time */
#include <unistd.h>    /* Unix std */
#include <sys/wait.h>

#include "system_utils.h"

#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>


/**
 * @brief Function to sort a file using an external sort and nb_split subfiles with the V0 project.
 * @param[in] i_file Name of the file to process.
 * @param[in] o_file Name of the output sorted file.
 * @param[in] nb_split Number of subfiles to use for the external sort.
 * @note No parallelisme.
 **/
void projectV0(const char *i_file, const char *o_file, unsigned long nb_split);

/**
 * @brief Function to sort a file using an external sort and nb_split subfiles with the V1 project.
 * @param[in] i_file Name of the file to process.
 * @param[in] o_file Name of the output sorted file.
 * @param[in] nb_split Number of subfiles to use for the external sort.
 * @note No parallelisme.
 **/
void projectV1(const char *i_file, const char *o_file, unsigned long nb_split);

/**
 * @brief Function to sort a file using an external sort and nb_split subfiles with the V2 project.
 * @param[in] i_file Name of the file to process.
 * @param[in] o_file Name of the output sorted file.
 * @param[in] nb_split Number of subfiles to use for the external sort.
 * @note No parallelisme.
 **/
void projectV2(const char *i_file, const char *o_file, unsigned long nb_split);

/**
 * @brief Function to sort a file using an external sort and nb_split subfiles with the V3 project.
 * @param[in] i_file Name of the file to process.
 * @param[in] o_file Name of the output sorted file.
 * @param[in] nb_split Number of subfiles to use for the external sort.
 * @note No parallelisme.
 **/
void projectV3(const char *i_file, const char *o_file, unsigned long nb_split);

/**
 * @brief Function to sort a file using an external sort and nb_split subfiles with the V4 project.
 * @param[in] i_file Name of the file to process.
 * @param[in] o_file Name of the output sorted file.
 * @param[in] nb_split Number of subfiles to use for the external sort.
 * @note No parallelisme.
 **/
void projectV4(const char *i_file, const char *o_file, unsigned long nb_split);



/**
 * @brief Function to sort a temporary subfile and remove it with the V0 project.
 * @param[in] nb_split Index of the subfile in the array of files.
 * @param[in] filenames Array of file to sort names.
 * @param[in] filenames_sort Array of sorted file names.
 **/
void projectV0_sortFiles(unsigned long nb_split,
                         const char **filenames,
                         const char **filenames_sort);

/**
 * @brief Function to sort a temporary subfile and remove it with the V1 project.
 * @param[in] nb_split Index of the subfile in the array of files.
 * @param[in] filenames Array of file to sort names.
 * @param[in] filenames_sort Array of sorted file names.
 **/
void projectV1_sortFiles(unsigned long nb_split,
                         const char **filenames,
                         const char **filenames_sort);

/**
 * @brief Function to sort a temporary subfile and remove it with the V2 project.
 * @param[in] nb_split Index of the subfile in the array of files.
 * @param[in] filenames Array of file to sort names.
 * @param[in] filenames_sort Array of sorted file names.
 **/
void projectV2_sortFiles(unsigned long nb_split,
                         const char **filenames,
                         const char **filenames_sort);

/**
 * @brief Function to sort a temporary subfile and remove it with the V3 project.
 * @param[in] nb_split Index of the subfile in the array of files.
 * @param[in] filenames Array of file to sort names.
 * @param[in] filenames_sort Array of sorted file names.
 **/
void projectV3_sortFiles(unsigned long nb_split,
                         const char **filenames,
                         const char **filenames_sort);

/**
 * @brief Function to sort a temporary subfile and remove it with the V4 project.
 * @param[in] nb_split Index of the subfile in the array of files.
 * @param[in] filenames Array of file to sort names.
 * @param[in] filenames_sort Array of sorted file names.
 **/
void projectV4_sortFiles(unsigned long nb_split,
                         const char **filenames,
                         const char **filenames_sort,const char *o_file);

/**
 * @brief Function to sort-merge a list of sorted subfiles with the V4 project.
 * @param[in] nb_split Index of the subfile in the array of files.
 * @param[in] filenames_sort Array of sorted file names.
 * @param[in] o_file Nome of the output file where sorted data are written.
 * @note It work in stream. Files are not fully loaded in memory.
 **/
void projectV0_combMerge(unsigned long nb_split,
                         const char **filenames_sort,
                         const char *o_file);

/**
 * @brief Function to sort-merge a list of sorted subfiles with the V4 project.
 * @param[in] nb_split Index of the subfile in the array of files.
 * @param[in] filenames_sort Array of sorted file names.
 * @param[in] o_file Nome of the output file where sorted data are written.
 * @note It work in stream. Files are not fully loaded in memory.
 **/
void projectV1_combMerge(unsigned long nb_split,
                         const char **filenames_sort,
                         const char *o_file);

/**
 * @brief Function to sort-merge a list of sorted subfiles with the V4 project.
 * @param[in] nb_split Index of the subfile in the array of files.
 * @param[in] filenames_sort Array of sorted file names.
 * @param[in] o_file Nome of the output file where sorted data are written.
 * @note It work in stream. Files are not fully loaded in memory.
 **/
void projectV2_combMerge(unsigned long nb_split,
                         const char **filenames_sort,
                         const char *o_file);

/**
 * @brief Function to sort-merge a list of sorted subfiles with the V4 project.
 * @param[in] nb_split Index of the subfile in the array of files.
 * @param[in] filenames_sort Array of sorted file names.
 * @param[in] o_file Nome of the output file where sorted data are written.
 * @note It work in stream. Files are not fully loaded in memory.
 **/
void projectV3_combMerge(unsigned long nb_split,
                         const char **filenames_sort,
                         const char *o_file);

/**
 * @brief Function to sort-merge a list of sorted subfiles with the V4 project.
 * @param[in] nb_split Index of the subfile in the array of files.
 * @param[in] filenames_sort Array of sorted file names.
 * @param[in] o_file Nome of the output file where sorted data are written.
 * @note It work in stream. Files are not fully loaded in memory.
 **/
void projectV4_combMerge(unsigned long nb_split,
                         const char **filenames_sort,
                         const char *o_file);



/**
 * @brif Structure to create a list of task.
 **/
typedef struct maillon {
    char  *info;
    struct maillon * suiv;
}T_maillon;

/**
 * @brief Function to add a task in a list.
 * @param f Element of the new list.
 * @param file  Name of a file.
 */
void ajoutTete(T_maillon *f, char* file);
#endif
