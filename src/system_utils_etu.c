/**
 * @file 
 * @brief Implementation by students of usefull function for the system project.
 * @todo Change the SU_removeFile to use exec instead of system.
 */


#include "system_utils.h"

/**
 * @brief Maximum length (in character) for a command line.
 **/
#define SU_MAXLINESIZE (1024*8) 

/********************** File managment ************/

void SU_removeFile(const char * file){
	pid_t fk;
	char buffer[SU_MAXLINESIZE];
	snprintf(buffer, SU_MAXLINESIZE, "%s",file);
	//fprintf(stderr, "%s\n", buffer);

	//Define command + arguments for deleting the file
	//char *command = "/bin/rm";
	//char *arguments[] = { "/bin/rm", buffer, (char *) 0};
	if((fk=fork())==-1){
		perror("fork() failed");
	}else if(fk== 0){
		execl("/bin/rm","rm",buffer,(char *)0);
	}else {
		wait(NULL);
	}
}

 