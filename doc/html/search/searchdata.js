var indexSectionsWithContent =
{
  0: "almpst",
  1: "m",
  2: "ps",
  3: "aps",
  4: "t",
  5: "ps",
  6: "ls"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "typedefs",
  5: "defines",
  6: "pages"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Structures de données",
  2: "Fichiers",
  3: "Fonctions",
  4: "Définitions de type",
  5: "Macros",
  6: "Pages"
};

