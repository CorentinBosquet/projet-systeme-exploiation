# Projet Système d'exploitation
## Trions ensemble

### Traitements

1. Le programme sépare en plusieurs parties le fichier initial (non parallélisable).
2. Il fait un tri interne sur ces "sous-fichiers".
3. Il prend deux fichiers et les fusionne.
4. Il prend le fichier fusionné et le fusionne avec un autre fichier.

### Les versions ajoutées
#### V1

Le tri des fichiers se fait en parallèle avec des processus lourds.

#### V2

La fusion des fichiers triés se fait en parallèle avec des processus lourds.

#### V3

Le tri et la fusion des fichiers se font en parallèle avec des processus légers.

#### V4

Dans une liste d'attente FIFO, on ajoute toutes les tâches en attente. les différentes tâches sont faites avec des processus légers.

### Commandes disponibles
Dans le dossier du projet, vous pouvez exécuter ces commandes :
- ./bin/project -m test
- ./bin/project -m generation -o /tmp/test.txt -n 50000000
- ./bin/project -m lineCount -i /tmp/test.txt
- ./bin/project -m demoSort -i /tmp/test.txt -o /tmp/test.sort.txt
- ./bin/project -m demoSortSplit -i /tmp/test.txt -o /tmp/test.sort.txt
- ./bin/project -m projectV0 -i /tmp/test.txt -o /tmp/test.sort.txt -k 5 
- ./bin/project -m projectV1 -i /tmp/test.txt -o /tmp/test.sort.txt -k 5 
- ./bin/project -m projectV2 -i /tmp/test.txt -o /tmp/test.sort.txt -k 5
- ./bin/project -m projectV3 -i /tmp/test.txt -o /tmp/test.sort.txt -k 5

Pour plus d'informations, tapez la commande 
./bin/project -h

Vous pouvez aussi consulter la documentation doxygen à l'emplacement : ./doc/html/index.html


## Auteurs
BOSQUET Corentin
GALLIOU Quentin

Apprentis ingénieurs
Informatique Multimédia et Réseaux
Ecole Nationale Supérieure des Sciences Appliquées et de Technologie

